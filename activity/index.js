
// 	Activity:

// 	1. Update and Debug the following codes to ES6.
// 		Use template literals,
// 		Use array/object destructuring,
// 		Use arrow functions
	
//     2. Create a class constructor able to receive 3 arguments
//         It should be able to receive 3 strings and a number
//         Using the this keyword assign properties:
//         username, 
//         role, 
//         guildName,
//         level 
//   assign the parameters as values to each property.
//   Create 2 new objects using our class constructor.
//   This constructor should be able to create Character objects.



// 	Create 2 new objects using our class constructor.

// 	This constructor should be able to create Dog objects.

// 	Log the 2 new Dog objects in the console.


// 	Pushing Instructions:

// 	Create a git repository named S24.
// 	Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 	Add the link in Boodle.

// */

//Solution: 

/*Debug*/

let student1 = {

	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101","Social Sciences 201"]

};

let student2 = {
	name: "Steve Austin",
	birthday:"June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
};


const {name: name2, birthday: birthday2, age: age2, isEnrolled: isEnrolled2, classes: classes2} = student2;
console.log(name2, birthday2, age2, isEnrolled2, classes2);









// /*Debug and Update*/

// function introduce(student){

// 	console.log("Hi! I'm " + student.name + "." + "I am " + student.ages + " years old.")
// 	console.log("I study the following courses: " + student.class)

// }

// introduce(student1);
// // introduce(student2);


const introduce = (student) => {
	console.log(`Hi! I'm ${student.name} I am + ${student.age} years old.` );
}
introduce(student1);
introduce(student2);






// function getCube(num){

const getCube = (numArr) =>{

	console.log(Math.pow(numArr,3));

}

let numArr = [15,16,32,21,21,2];


numArr.forEach(function(num){
	console.log(num ** 3);
})

getCube();
console.log(numArr);



let numsSquared = numArr.map(function(num){

	return num ** 2;

  }
)


console.log(numsSquared);



//2. Class Constructor

class Dog {
	constructor(username,role,guildName,level){

	this.username = username;
	this.role = role;
	this.guildName = guildName;
	this.level = level;


	}
}

let Dog1 = new Dog ("George", "guard", "Dogzaur", 107);
console.log(Dog1);

let Dog2 = new Dog ("Ranger", "Fighter", "PawpawRanger", 500);
console.log(Dog2);


