//EXPONENT OPERATOR
/*
FORMULAS

1. Math.pow(base, exponent);
2. updated exp operator (base ** exponent);

*/

let fivePowerOf3 = Math.pow(5,3);
console.log(fivePowerOf3);

//RESULT = 125 ( 5 *5 *5)

let fivePowerOf2 = 5**2;
console.log(fivePowerOf2);


//SQUARE ROOT
//Syntax: base**.5

let squareRootof16 = 16**.5;
console.log(squareRootof16);


//MINI ACTIVITTY

let string1 = "Javascript";
let string2 = "not";
let string3 = "is";
let string4 = "Typescript";
let string5 = "Java";
let string6 = "Zuitt";
let string7 = "Coding";



// let sentence1 = string1 + " " + string3 + " " + string2 + " "+ string5;
// console.log(sentence1);


let sentence2 = string4 + " " + string3 + " " + string1;
console.log(sentence2);



//" " or ' ' - string literals
//TEMPLATE LITERALS = `${}  = placeholder used to embed JS expression when creating strings using Template Literals


let sentence1 = `${string1} ${string3} ${string2} ${string5}`;
console.log(sentence1);


// use template literals to add a new sentenmce in our sentence3 variables
let sentence3 = `${string6} ${string7} Bootcamp`;
console.log(sentence3);

let sentence4 = `The sum of 15 and 25 is ${15+25}`;
console.log(sentence4);



let person = {
	name: "Michael",
	position: "developer",
	income: 50000,
	expenses: 60000
}

console.log(`${person.name} is a ${person.position}`);

console.log(`His income is ${person.income} and expenses at ${person.expenses}. His current balance is ${person.income - person.expenses}`);


//DESTRUCTURING ARRAYS AND OBJECTS - will allow us to save aray items or object properties into new variables
// w/out having to create/initialize with accessing the items/or properties one by one


//sample1
let array1 = ["Curry", "Lillard", "Paul", "Irving"];

// let player1 = array[0];
// let player2 = array[1]
// let player3 = array[2]
// let player4 = array[3]

let [player1,player2,player3,player4] = array1;

console.log(player1,player2,player3,player4);


//sample2

let array2 = ["Jokic", "Embiid", "Howard", "Anthony-Towns"];

// let [center1,center2,center3] = array2;


let [center1,center2,,center4] = array2; //use ,, = to skip an element

console.log(center4);




//OBJECT DESTRUCTURING - ORDER OF DESstructuring does not matter, the name of the variable must match a property in the object

let pokemon1 = {

	name: "Bulbasaur",
	type: "Grass",
	level: 10,
	moves: ["Razor Leaf", "Tackle", "Leech Seed"]

}


let {level,type,name,moves,personality} = pokemon1;

console.log(level);
console.log(type);
console.log(name);
console.log(moves);
console.log(personality);




let pokemon2 = { //error when two variables have same properties

	name: "Charmander",
	type: "Fire",
	level: 11,
	moves: ["Ember", "Scratch"]

}

const {name: name2} = pokemon2;
console.log(name2);



//ARROW FUNCTIONS -alternative way of writing function in javascript however there are pros and cons between traditional and arrow functions

//TRADITIONAL FUNC

function displayMsg(){
	console.log("Hello, world!");
}
displayMsg();


//ARROW FUNCTION'
const hello = () => {
	console.log('Hello from arrow');
}
hello();


//ARROW FUNC W/ PARAMETERS

const greet = (friend) => {

	// console.log(friend); --- check first
	console.log(`Hi! ${friend.name}`);
}

greet(person); //person - info line 64



// ARROW VS TRADITIONAL FUNC

//IMPLICIT RETURN = allows us to return a value from an arrow func w.out the use of return key word

function addNum(num1,num2){

	let result = num1 + num2;
	return result; // or return num1 + num2;
}

// let sum = addNum(5,10);
// console.log(sum);


//IMPLICIT RETURN 	- WILL RETURN EVEN WITHOUT {}


let subNum = (num1, num2) => num1 - num2;   //OR { return num1 - num2};
let difference = subNum(10,5);
console.log(difference);




// MINI ACTIVITY

// Translate this into implicit

		// let sum = addNum(50,70);
		// console.log(sum);

let addNnum= (num1,num2) => num1 + num2;  //or instead of let we can use const
let sum = addNum(50,70);
console.log(sum);



//TRADITIONAL VS ARROW

//sample 1

/*let character1 = {
	name: "Cloud Strife",
	oocupation: "SOLDIER",
	greet: function(){

		console.log(this);
		console.log(`Hi I'm ${this.name}`);
	}
}

character1.greet();*/



//sample 2 with arrow (this)

let character1 = {
	name: "Cloud Strife",
	oocupation: "SOLDIER",
	greet: function(){

		console.log(this);
		console.log(`Hi I'm ${this.name}`);

	},
	introduceJob: () => {
		console.log(this); // in an arrow function as method (this) keyword will not refer to the current object. instead it will refer to the global window object.
	}
}

character1.greet();
character1.introduceJob();




//CLASS BASED BLUEPRINTS
/*
classes are templates of objects. We can create classes out of the use of classes.
we mimic the behavior of being able to create objects of the same blueprint using constructor function
*/


function pokemon(name,type,level){

	this.name = name;
	this.type = type;
	this.level = level;
}


//V ==== E ====== R ======= S ====== U ====== S


class Car { //captial letter = naming convention for class or PascalCase
	constructor(brand,name,year){

		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

let Car1= new Car("Toyota", "Vios", "2002");
let Car2= new Car("Cooper", "Mini", "1969");
let Car3= new Car("Porsche", "911", "1967");

console.log(Car1);
console.log(Car2);
console.log(Car3);




//MINI ACTIVITY
/*

TRANSLATE THE POKEMON CONSTRUCTOR FUNCTION AS A CLASS CONSTRUCTOR THEN CREATE 2 POKEMOS OUT OF THE CLASS CONSTRUCTOR AND SAVE IT IN THEIRR RESPECTIVE VARIABES.

*/



class Pokemon {
	constructor(name,type,level,moves){

	this.name = name;
	this.type = type;
	this.level = level;
	this.noves = moves;


	}
}

let Pokemon1 = new Pokemon ("Bulbasaur", "Grass", 10, ["Razor leaf", "tackle", "Leech Seed"]);
console.log(Pokemon1);

let Pokemon2 = new Pokemon ("Charmander", "Fire", 11, ["Ember", "scratch"]);
console.log(Pokemon2);